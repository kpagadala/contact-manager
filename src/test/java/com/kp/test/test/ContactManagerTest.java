package com.kp.test.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import com.kp.test.ContactManager;
import com.kp.test.ContactManagerAdvanced;
import com.kp.test.DistanceService;

@TestInstance(Lifecycle.PER_CLASS)
public class ContactManagerTest {

	ContactManager contactManager;

	@BeforeAll
	public void setupAll() {
		System.out.println("SetupAll executes before all steps");
	}

	@BeforeEach
	public void setup() {
		System.out.println("Before each test");
		contactManager = new ContactManager();

	}

	@Test
	public void addContactTest() {
		contactManager.addContact("kk", "pp", "0123456789");
		assertEquals(1, contactManager.getAllContacts().size());
		assertTrue(contactManager.getAllContacts().stream()
				.filter(c -> c.getFirstName().equals("kk") && c.getLastName().equals("pp")).findAny().isPresent());

	}
	
	@ParameterizedTest
	@ValueSource(strings= {"0123456789", "0123456789", "012345678"})
	public void addContactParameterizedTest(String phoneNumber) {
		contactManager.addContact("kk", "pp", phoneNumber);
		assertEquals(1, contactManager.getAllContacts().size());
		assertTrue(contactManager.getAllContacts().stream()
				.filter(c -> c.getFirstName().equals("kk") && c.getLastName().equals("pp")).findAny().isPresent());

	}
	
	@ParameterizedTest
	@MethodSource("phoneNumbers")
	public void addContactParameterizedMethodTest(String phoneNumber) {
		contactManager.addContact("kk", "pp", phoneNumber);
		assertEquals(1, contactManager.getAllContacts().size());
		assertTrue(contactManager.getAllContacts().stream()
				.filter(c -> c.getFirstName().equals("kk") && c.getLastName().equals("pp")).findAny().isPresent());

	}
	
	private static List<String> phoneNumbers() {
		return Arrays.asList("0123456789", "0123456789", "0123456789");
	}
	
	@ParameterizedTest
	@CsvSource({"0123456789", "0123456789", "0123456789"})
	public void addContactParameterizedCsvTest(String phoneNumber) {
		contactManager.addContact("kk", "pp", phoneNumber);
		assertEquals(1, contactManager.getAllContacts().size());
		assertTrue(contactManager.getAllContacts().stream()
				.filter(c -> c.getFirstName().equals("kk") && c.getLastName().equals("pp")).findAny().isPresent());

	}
	
	@ParameterizedTest
	@CsvFileSource(resources = "/data.csv")
	public void addContactParameterizedCsvFileTest(String phoneNumber) {
		contactManager.addContact("kk", "pp", phoneNumber);
		assertEquals(1, contactManager.getAllContacts().size());
		assertTrue(contactManager.getAllContacts().stream()
				.filter(c -> c.getFirstName().equals("kk") && c.getLastName().equals("pp")).findAny().isPresent());

	}

	@Test
	@DisplayName("Enabled on Windows")
	@EnabledOnOs(value=OS.WINDOWS)
	public void addContactTestOS() {
		contactManager.addContact("kk", "pp", "0123456789");
		assertEquals(1, contactManager.getAllContacts().size());
		assertTrue(contactManager.getAllContacts().stream()
				.filter(c -> c.getFirstName().equals("kk") && c.getLastName().equals("pp")).findAny().isPresent());

	}
	
	@DisplayName("Repeat test for 5 times")
	@RepeatedTest(value=5, name= "Repeat contact creation {currentRepetition} of {totalRepetitions}")
	public void addContactTestRepeated() {
		contactManager.addContact("kk", "pp", "0123456789");
		assertEquals(1, contactManager.getAllContacts().size());
		assertTrue(contactManager.getAllContacts().stream()
				.filter(c -> c.getFirstName().equals("kk") && c.getLastName().equals("pp")).findAny().isPresent());

	}
	
	@Test
	@DisplayName("Dont add contact when first name is null")
	public void firstNameRunTimeException() {
		assertThrows(RuntimeException.class, () -> contactManager.addContact(null, "pp", "0123456789"));

	}

	@Test
	@DisplayName("Dont add contact when last name is null")
	public void lastNameRunTimeException() {
		assertThrows(RuntimeException.class, () -> contactManager.addContact("kk", null, "0123456789"));

	}
	
	
	@Test
	public void addContactMock() {
		
		ContactManagerAdvanced contactManagerAdvanced = new ContactManagerAdvanced(new DistanceService());
		contactManagerAdvanced.addContact("kkk", "ppp", "0123456789");
		
		contactManagerAdvanced.getAllContacts().forEach(c -> System.out.println(c.getDistanceFromMe()));
		
	}
	
	

	@AfterEach
	public void teardown() {
		System.out.println("after each test");
	}

	@AfterAll
	public void teardownAll() {
		System.out.println("after all tests are done");
	}

}
