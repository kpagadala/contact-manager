package com.kp.test.test;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.kp.test.ContactManagerAdvanced;
import com.kp.test.DistanceService;

@ExtendWith(MockitoExtension.class)
public class ContactManagerAdvancedTest {

//	DistanceService distanceService = new DistanceService();

	@Mock
	DistanceService distanceService;

	@InjectMocks
	ContactManagerAdvanced contactManagerAdvanced;

	@Test
	public void addContactMock() {

//		ContactManagerAdvanced contactManagerAdvanced = new ContactManagerAdvanced(distanceService);
		when(distanceService.getDistanceFromMe(Mockito.anyString())).thenReturn(1);
		contactManagerAdvanced.addContact("kkk", "ppp", "0123456789");

		contactManagerAdvanced.getAllContacts().forEach(c -> System.out.println(c.getDistanceFromMe()));

	}

}
