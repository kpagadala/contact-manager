package com.kp.test;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ContactManagerAdvanced {

	DistanceService distanceService;

	public ContactManagerAdvanced(DistanceService distanceService) {
		this.distanceService = distanceService;
	}

	Map<String, Contact> contactList = new ConcurrentHashMap<String, Contact>();

	public void addContact(String firstName, String lastName, String phoneNumber) {
		Contact contact = new Contact(firstName, lastName, phoneNumber);
		validateContact(contact);
		checkIfContactAlreadyExist(contact);
//		Integer distance = distanceService.getDistanceFromMe(phoneNumber);
		contact.setDistanceFromMe(distanceService.getDistanceFromMe(phoneNumber) < 10 ? "local" : "national");
		contactList.put(generateKey(contact), contact);
	}
	public Collection<Contact> getAllContacts() {
		return contactList.values();
	}

	private void checkIfContactAlreadyExist(Contact contact) {
		if (contactList.containsKey(generateKey(contact)))
			throw new RuntimeException("Contact Already Exists");
	}

	private void validateContact(Contact contact) {
		contact.validateFirstName();
		contact.validateLastName();
		contact.validatePhoneNumber();
	}

	private String generateKey(Contact contact) {
		return String.format("%s-%s", contact.getFirstName(), contact.getLastName());
	}
}
